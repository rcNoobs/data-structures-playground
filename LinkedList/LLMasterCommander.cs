using System;
using LinkedListDS;

public class LLMasterCommander: IMasterCommander
{
    public LinkedList list {get; set;}
    public bool ShouldExit {get; set;}

    public LLMasterCommander()
    {
        list = new LinkedList();
        ShouldExit = false;
    }

    public void Execute()
    {
        while (!ShouldExit)
        {
            PrintOptions();
            ResolveInput(Console.ReadLine().ToLower());
        }
    }
    public void PrintOptions()
    {
        Console.WriteLine("\n");
        Console.WriteLine("Please enter a command:");
        Console.WriteLine("A.- Generate Random List (Clears Previous List)");
        Console.WriteLine("B.- Insert Node at Head");
        Console.WriteLine("C.- Insert Node at Tail");
        Console.WriteLine("D.- Insert Node at N");
        Console.WriteLine("E.- Remove Node at N");
        Console.WriteLine("F.- Remove Node with value");
        Console.WriteLine("G.- Invert List");
        Console.WriteLine("H.- Print List");
        Console.WriteLine("I.- Find Position of value");
        Console.WriteLine("Z.- Return to main menu");
    }

    public void ResolveInput(string input)
    {
        switch (input)
        {
            case "a":
            list.Clear();
            generateRandomList();
            break;
            case "b":
            insertAtHead();
            break;
            case "c":
            insertAtTail();
            break;
            case "d":
            insertAtPosition();
            break;
            case "e":
            removeAtPosition();
            break;
            case "f":
            removeNodeWithValue();
            break;
            case "g":
            list.Invert();
            list.Print();
            break;
            case "h":
            list.Print();
            break;
            case "i":
            findPositionOfNode();
            break;
            case "z":
            ShouldExit = true;
            break;
            default:
            Console.WriteLine("Unrecognized Option");
            break;
        }
    }

    private void generateRandomList()
    {
        var random = new Random();
        var n = ReadNumber("Enter number of nodes");
        Console.WriteLine($"Generating a new list of {n} random nodes");
        int i = 0;
        while (i<n)
        {
            list.InsertAtTail(random.Next(-n, n));
            i++;
        }
        list.Print();
    }

    private void insertAtHead()
    {
        var n = ReadNumber("Enter a value for the node to be inserted");
        list.InsertAtHead(n);
        list.Print();
    }

    private void insertAtTail()
    {
        var n = ReadNumber("Enter a value for the node to be inserted");
        list.InsertAtTail(n);
        list.Print();
    }

    private void insertAtPosition()
    {
        var n = ReadNumber("Enter a value for the node to be inserted");
        var m = ReadNumber("Enter the position in the list (Index starts at 0)");
        list.InsertAtPosition(n, m);
        list.Print();
    }

    private void removeAtPosition()
    {
        var n = ReadNumber("Enter the position in the list to be removed (Index starts at 0)");
        list.RemoveAtPosition(n);
        list.Print();
    }

    private void removeNodeWithValue()
    {
        var n = ReadNumber("Enter the value of the node to be removed");
        list.RemoveWithValue(n);
        list.Print();
    }

    private void findPositionOfNode()
    {
        var n = ReadNumber("Enter a value");
        var pos = list.PositionOf(n);

        Console.WriteLine( pos > 0 ? $"A node with value {n} was found at position {pos}" : $"Could not find a Node with value {n}");
    }

    private int ReadNumber(string message)
    {
        Console.WriteLine(message);
        while (true)
        {
            try
            {
                var num = Convert.ToInt32(Console.ReadLine());
                return num;
            }
            catch (System.Exception)
            {
                Console.WriteLine("Invalid Input, please specify a number");
            }
        }
    }
}