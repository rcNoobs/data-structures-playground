namespace LinkedListDS {
    public class Node {
        public Node(){
            Value = 0;
            Next = null;
        }
        public Node(int val){
            Value = val;
        }
        public int Value {get; set;}
        public Node Next {get; set;}
    }
}