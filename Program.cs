﻿using System;

namespace DataStructures
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var input = string.Empty;
            
            Console.WriteLine("Hello! This is a Data Structures Playground in C#");
            Console.WriteLine("By Daniel Ormeno");

            while(!input.Equals("exit", StringComparison.CurrentCultureIgnoreCase))
            {
                PrintOptions();
                input = Console.ReadLine().ToLowerInvariant();
                ResolveInput(input);
            }
        }

        public static void PrintOptions(){
            Console.WriteLine("Please enter a command:");
            Console.WriteLine("A.- LinkedLists");
            Console.WriteLine("B.- Binary Tree");
            Console.WriteLine("Exit.- To Exit playground");
        }

        public static void ResolveInput(string input){
            switch (input)
            {
                case "a":
                var ll = new LLMasterCommander();
                ll.Execute();
                break;
                case "b":
                var bt = new BTMasterCommander();
                bt.Execute();
                break;
                case "exit":
                Console.WriteLine("Bye Bye");
                break;
                default:
                Console.WriteLine("Unrecognized command");
                break;
            }
        }
    }
}
