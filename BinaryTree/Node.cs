namespace BinaryTreeDS {
    public class Node 
    {
        public Node(int val){
            Value = val;
        }
        
        public Node Left {get; set;}
        public Node Right {get; set;}
        public int Value {get; set;}
    }
}