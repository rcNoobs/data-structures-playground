using System;
using BinaryTreeDS;
public class BTMasterCommander : IMasterCommander
{
    public bool ShouldExit {get; set;}
    private BinaryTree tree {get; set;}

    public BTMasterCommander()
    {
        tree = new BinaryTree();
    }

    public void Execute()
    {
        while (!ShouldExit)
        {
            PrintOptions();
            ResolveInput(Console.ReadLine().ToLower());
        }
    }

    public void PrintOptions()
    {
        Console.WriteLine("\n");
        Console.WriteLine("Please enter a command:");
        Console.WriteLine("A.- Generate Random Tree (Clears Previous DS)");
        Console.WriteLine("B.- Find Node with Value");
        Console.WriteLine("C.- Insert Node");
        Console.WriteLine("D.- Remove Node");
        Console.WriteLine("E.- Print Tree");
        Console.WriteLine("Z.- Return to main menu");
    }

    public void ResolveInput(string input)
    {
        switch (input)
        {
            case "a":
            tree.Clear();
            generateRandomTree();
            break;
            case "b":
            findNodeWithValue();
            break;
            case "c":
            insertNodeWithValue();
            tree.Print();
            break;
            case "d":
            removeNodeWithValue();
            tree.Print();
            break;
            case "e":
            tree.Print();
            break;
            case "z":
            ShouldExit = true;
            break;
            default:
            Console.WriteLine("Unrecognized Option");
            break;
        }
    }

    private void findNodeWithValue()
    {
        var n = ReadNumber("Enter the value for the search");
        var result = tree.FindValue(n);

        if (result == null)
        {
            Console.WriteLine($"Could Not find node with value {n}");
        }
        else 
        {
            tree.PrintNode(result);
        }
    }

    private void generateRandomTree()
    {
        Random random = new Random();
        var n = ReadNumber("Enter number of nodes");
        Console.WriteLine($"Generating a new tree of {n} random nodes");
        int i = 0;
        while (i<n)
        {
            tree.Insert(random.Next(-n, n));
            i++;
        }
        tree.Print();
    }

    private void insertNodeWithValue()
    {
        var n = ReadNumber("Enter a value to insert in the tree");
        tree.Insert(n);
    }

    private void removeNodeWithValue()
    {
        var n = ReadNumber("Enter a value to remove in the tree");
        tree.Remove(n);
    }

    private int ReadNumber(string message)
    {
        Console.WriteLine(message);
        while (true)
        {
            try
            {
                var num = Convert.ToInt32(Console.ReadLine());
                return num;
            }
            catch (System.Exception)
            {
                Console.WriteLine("Invalid Input, please specify a number");
            }
        }
    }
}